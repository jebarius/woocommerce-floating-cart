<style>
.woo-floating-cart-form{
	max-width: 600px;
	background:#fff;
	border:1px solid #ddd;
	padding:15px;
	margin-top: 15px;
	-webkit-border-radius: 7px;
	border-radius: 7px;
}
.woo-floating-cart-form label{
	display: block;
	margin-bottom: 5px;
	font-weight: bold;
}
.woo-floating-cart-form hr{
	border-top: 1px solid #ddd;
    border-bottom: 1px solid #fafafa;
}

.form-block{
	margin-bottom: 5px;
	margin-top: 10px;
	display: block;
}
.form-block.hide-field{
	display: none;
}
.woo-floating-cart-form label p{
	display: block;
	opacity: .8;
	margin:0;
	font-weight: normal;

}

.woo-floating-cart-form label select,
.woo-floating-cart-form label input,
.woo-floating-cart-form label textarea{
	display: block;
	max-width: 300px;
	font-weight: normal;
}
</style>

<?php 
	$data = $this->get_field_data();
?>

<form method="POST" action="#" class="woo-floating-cart-form">
	<?php foreach ($data as $key => $value) { 
		$visibility = '';
		if(isset($value['visibility']) && ($data[$value['visibility']['key']]['value'] !== $value['visibility']['value'])){
			$visibility = 'hide-field';
		}
		?>
		<div class="form-block <?php echo $visibility; ?>">
		<?php if($value['type'] === 'select'){ ?>
				<label><?php echo $value['label']; ?>
				<p><?php echo $value['help']; ?></p>
				<select name="<?php echo $key; ?>">
					<?php foreach ($value['options'] as $subkey => $subvalue) { ?>
						<option value="<?php echo $subvalue['value']; ?>" <?php if($value['value'] === $subvalue['value']){ echo 'selected="selected"'; }?>><?php echo $subvalue['label']; ?></option>
					<?php } ?>
					
				</select>
			</label>
			
			<?php }else if($value['type'] === 'colour'){ ?>
				<label><?php echo $value['label']; ?>
				<p><?php echo $value['help']; ?></p>
				
			</label>

			<input type="text" class="color-field" name="<?php echo $key; ?>" value="<?php echo $value['value']; ?>">
			
			<?php

			}else if($value['type'] === 'spacer'){ ?> 
				<br />
				<h3><?php echo $value['title']; ?></h3>
				<hr />
			<?php }else if($value['type'] === 'info'){ ?> 

				<h3><?php echo $value['title']; ?></h3>
				<p><?php echo $value['content']; ?></p>
			<?php } ?>


		</div>
	<?php } ?>



	<input type="hidden" name="woo-floating-cart-save" value="1">
	<div class="form-block">
		<br />
		<hr />
		<button type="submit" class="button-primary">Save</button>	
	</div>
</form>



<script>
jQuery(document).ready(function(){

	jQuery('.color-field').wpColorPicker();
});
</script>