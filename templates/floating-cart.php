<?php
	/**
	 *
	 * Simple Floating Cart Button.
	 * - redirects straight through to cart.
	 *
	 */

	global $woocommerce;
	$cart_count = $woocommerce->cart->cart_contents_count;
	$cart_url = $woocommerce->cart->get_cart_url();
	
	$data = $this->get_field_data();

?>
<style>
	#woo-floating-cart{
		background-color:<?php echo $data['button-bg']['value'];?>;
		color:<?php echo $data['button-icon']['value'];?>;
	}
	#woo-floating-cart .woo-floating-cart-item-count{
		background-color:<?php echo $data['button-count-bg']['value'];?>;
		color:<?php echo $data['button-count-text']['value'];?>;
	}
</style>

<div id="woo-floating-cart" class="position-<?php echo $data['position']['value']; ?>">
	<span class="woo-floating-cart-item-count"><span class="woo-floating-cart-item-count-val"><?php echo $cart_count;?></span></span>
	<a href="<?php echo $cart_url;?>"><i class="fas fa-shopping-cart"></i></a>
</div>
