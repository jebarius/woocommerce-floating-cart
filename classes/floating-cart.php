<?php 


class WoocommerceFloatingCart {

	public $template;
	public $adminTemplate;
	public $templatePath;
	public $fields = [
			'spacer1' => [
				'type' => 'info',
				'title' => 'Woocommerce - Floating Cart',
				'content' => 'Thank you for downloading Woocommerce Floating Cart. This plugin is used to add a small floating button to every page on your wordpress site which will allow the user to go straight through to their basket.'
			],
			'spacer2' => [
				'title' => 'Configuration',
				'type' => 'spacer'
			],
			'status' => [
				'type' => 'select',
				'value' => '0',
				'label' => 'Enable floating cart button',
				'help' => "This allows you to enable the plugin without it automaticly showing the floating button before you've configured it.",
				'options' => [
					[
						'value' => '0',
						'label' => 'Disabled'
					],
					[
						'value' => '1',
						'label' => 'Enabled'
					]
				],
			],
			'debug' => [
				'type' => 'select',
				'value' => '0',
				'label' => 'Debug Mode',
				'help' => "If this option is enabled the button will only be visible to admins.",
				'visibility' => [
					'key' => 'status',
					'value' => '1'
				],
				'options' => [
					[
						'value' => '0',
						'label' => 'Disabled'
					],
					[
						'value' => '1',
						'label' => 'Enabled'
					]
				],
			],
			'spacer' => [
				'title' => 'Styling',
				'type' => 'spacer',
				'visibility' => [
					'key' => 'status',
					'value' => '1'
				],
			],
			'position' => [
				'type' => 'select',
				'value' => 'bottom-right',
				'label' => 'Button position',
				'help' => "This option controls where the button shows on the screen.",
				'visibility' => [
					'key' => 'status',
					'value' => '1'
				],
				'options' => [
					[
						'value' => 'top-left',
						'label' => 'Top Left'
					],
					[
						'value' => 'top-right',
						'label' => 'Top Right'
					],
					[
						'value' => 'bottom-left',
						'label' => 'Bottom Left'
					],
					[
						'value' => 'bottom-right',
						'label' => 'Bottom Right'
					]
				]
			],
			'button-bg' => [
				'type' => 'colour',
				'value' => '#000000',
				'label' => 'Button Background Colour',
				'help' => "This input allows you to set the background colour of the floating cart button",
				'visibility' => [
					'key' => 'status',
					'value' => '1'
				],	
			],
			'button-icon' => [
				'type' => 'colour',
				'value' => '#ffffff',
				'label' => 'Button Icon Colour',
				'help' => "This input allows you to set the icon colour of the floating cart button",	
				'visibility' => [
					'key' => 'status',
					'value' => '1'
				],
			],
			'button-count-bg' => [
				'type' => 'colour',
				'value' => '#e84118',
				'label' => 'Cart Count Background Colour',
				'help' => "This input allows you to set the background colour for the counter (if it is enabled).",	
				'visibility' => [
					'key' => 'status',
					'value' => '1'
				],
			],
			'button-count-text' => [
				'type' => 'colour',
				'value' => '#ffffff',
				'label' => 'Cart Count Colour',
				'help' => "This input allows you to set the colour for the counter (if it is enabled).",
				'visibility' => [
					'key' => 'status',
					'value' => '1'
				],
					
			],
	];

	public function __construct($config) {
		//get template values
		$this->template = $config['template'];
		$this->adminTemplate = $config['adminTemplate'];
		$this->templatePath = $config['templatePath'];

		//hook into footer and add template
		add_action( 'wp_footer', [$this,'floating_cart'] );
		//enqueue stylesheet
		add_action('wp_enqueue_scripts', [$this,'enqueue_scripts']);

		//setup admin section
		add_action('admin_menu', [$this,'floating_cart_admin']);
		add_action('admin_enqueue_scripts', [$this,'admin_enqueue_scripts'] );
		add_filter('woocommerce_add_to_cart_fragments', [$this,'floating_cart_update'], 10, 1 );

		//get field data
		$this->fields = $this->get_field_data();
		//if post data submitted update fields
		if(isset($_POST['woo-floating-cart-save'])){
			$this->update_data();
		}

	}

	/**
	 *
	 * Update floating cart config data
	 *
	 */
	public function update_data(){
		//loop through $this->fields and update option data
		foreach ($_POST as $key => $value) {
			if(isset($this->fields[$key])){
				update_option( 'woo-floating-cart-'.$key, $value);
			}
		}

	}

	/**
	 *
	 * Get floating cart config data
	 *
	 */

	public function get_field_data(){
		foreach ($this->fields as $key => $value) {
			$this->fields[$key]['value'] = get_option( 'woo-floating-cart-'.$key, $this->fields[$key]['value'] );
		}

		return $this->fields;
	}

	/**
	 *
	 * Render Floating Cart
	 *
	 */

	public function floating_cart() {
		$show = true;

		//if enabled, if debug enabled if user is not admin. hide button
		if($this->fields['status']['value'] == '1' && $this->fields['debug']['value'] == '1'){
			if(!current_user_can('administrator')){
				$show = false;
			}
		}

		if($show){
			if ( ! function_exists( 'is_woocommerce_activated' ) ) {
				$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		        if ( ! WC()->cart->is_empty()) { 
		        	if (!strpos($url,'/cart') && !strpos($url,'/checkout')) {
		        		require_once($this->template);
					}
		        	
		 		}
		 	}
		}
 	}

 	/**
 	 *
 	 * enqueue scripts and styles for the frontend
 	 *
 	 */

 	public function enqueue_scripts() {
		wp_enqueue_style('woo-floating-cart', plugin_dir_url('woocommerce-floating-cart/assets').'assets/style.css');
	}

	/**
	 *
	 * Queue admin scripts if user is admin
	 *
	 */

	public function admin_enqueue_scripts(){
		if( is_admin() ) { 
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_script( 'wp-color-picker-script-handle', plugins_url('assets/colour-picker.js'), array( 'wp-color-picker' ), false, true );
		}
	}

	/**
	 *
	 * Add admin menu item and page
	 *
	 */

	public function floating_cart_admin() {
	  add_submenu_page( 'woocommerce', 'Floating Cart Configuration', 'Floating Cart', 'manage_options', 'woo-floating-cart-configuration', [$this,'floating_cart_admin_page']);
	}

	/**
	 *
	 * Render admin page
	 *
	 */

	public function floating_cart_admin_page(){
	  //content on page goes here
		require_once($this->adminTemplate);
	}

	/**
	 *
	 * Function hooks into ajax add to cart and updates cart count.
	 *
	 */
	
	public function floating_cart_update( $fragments ) {
	    global $woocommerce;
		$cart_count = $woocommerce->cart->cart_contents_count;
		
	    $fragments['span.woo-floating-cart-item-count-val'] = '<span class="woo-floating-cart-item-count-val">' . $cart_count . '</span>';
	    
	    return $fragments;
	    
	}

}
