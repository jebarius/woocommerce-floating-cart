<?php
/**
 * @package Woocommerce_Floating_Cart
 * @version 0.0.2
 */
/*
Plugin Name: Woocommerce Floating Cart
Plugin URI: #
Description: Add a simple floating button that shows the number of products in a customers cart. Requires WooCommerce
Author: James Bell
Version: 0.0.2
Author URI: http://jebari.us
*/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

//include floating cart class
require_once(plugin_dir_path( __FILE__ ) . 'classes/floating-cart.php');

//initialize foating cart plugin
$plugin = new WoocommerceFloatingCart([
	"template" => plugin_dir_path( __FILE__ ) . 'templates/floating-cart.php',
	"templatePath" => plugin_dir_path( __FILE__ ) . 'templates/',
	"adminTemplate" => plugin_dir_path( __FILE__ ) . 'admin/admin.php',
]);
